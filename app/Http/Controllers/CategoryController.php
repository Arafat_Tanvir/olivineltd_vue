<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Image;
use File;

class CategoryController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $category=Category::get();
         return response()->json($category,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,category $category)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'image'=>'required'
        ]);

        $category=new Category;
        $category->name=$request->name;
        $category->description=$request->description;
        if($request->image)
        {
            $image=time().'.'.explode('/',explode(':',substr($request->image,0,strpos($request->image,';')))[1])[1];
            \Image::make($request->image)->save(public_path('images/categories/'.$image));
            $category->image=$image;
        }
        $category->save();
        return response()->json($category,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return response()->json($category,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        $category = Category::findOrFail($id);
        $category->name=$request->name;
        $category->description=$request->description;
        if($request->image)
        {
            $image=time().'.'.explode('/',explode(':',substr($request->image,0,strpos($request->image,';')))[1])[1];
            \Image::make($request->image)->save(public_path('images/categories/'.$image));
            $category->image=$image;
        }
        
        $category->update();
        return response()->json($category,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $category=Category::find($id);
        $category->delete();
        return response()->json($category,200);
    }
}
