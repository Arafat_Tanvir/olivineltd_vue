<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix'=>'categories'],function(){
    Route::get('',"CategoryController@index")->name('category-index');
    Route::get('/create',"CategoryController@create")->name('category-create');
    Route::post('/store',"CategoryController@store")->name('category-store');
    Route::get('/show/{id}',"CategoryController@show")->name('category-show');
    Route::get('/edit/{id}',"CategoryController@edit")->name('category-edit');
    Route::post('/update/{id}',"CategoryController@update")->name('category-update');
    Route::post('/delete/{id}',"CategoryController@delete")->name('category-delete');
});
