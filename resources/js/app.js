require('./bootstrap');
window.Vue = require('vue');

//for v router use
import VueRouter from 'vue-router'
Vue.use(VueRouter);

//for vform uses
window.Form=Form;
import { Form, HasError, AlertError } from 'vform'
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

//sweet aller
import swal from 'sweetalert2'
window.swal=swal;

const Toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer);
        toast.addEventListener('mouseleave', swal.resumeTimer)
    }
});
window.Toast=Toast;



//all component
let routes = [
    { path: '/dashboard', component: require('./components/dashboardComponent.vue').default },
    { path: '/profile', component: require('./components/profileComponent.vue').default },
    { path: '/categories-index', component: require('./components/categories/IndexComponent').default },
    { path: '/categories-create', component: require('./components/categories/CreateComponent.vue').default },
];
const router=new VueRouter({
    mode: 'history',
    routes
});


const app = new Vue({
    el: '#app',
    router
});
